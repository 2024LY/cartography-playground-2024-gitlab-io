require 'bundler'
require 'bundler/lockfile_parser'

def gem_dependencies
  # Load Gemfile.lock
  lockfile = Bundler::LockfileParser.new(Bundler.read_file('Gemfile.lock'))
  
  # Build dependencies hash
  dependencies = lockfile.specs.each_with_object({}) do |spec, hash|
    hash[spec.name] = spec.dependencies.map(&:name)
  end
  
  dependencies
end

def generate_dot(dependencies)
  File.open('dependencies.dot', 'w') do |file|
    file.puts("digraph G {")
    dependencies.each do |gem, deps|
      deps.each do |dep|
        file.puts("  \"#{gem}\" -> \"#{dep}\";")
      end
    end
    file.puts("}")
  end
end

dependencies = gem_dependencies
generate_dot(dependencies)
