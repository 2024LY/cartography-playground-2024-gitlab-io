//==============================================================================
//  GENERAL STUFF
//==============================================================================

// primary color from golbal css
// var color_primary = getComputedStyle(document.body).getPropertyValue("--primary");
var fontButton = document.getElementById("fontMenuButton");
var fontMenu = document.getElementById("fontMenu");
var sizeButton = document.getElementById("sizeMenuButton");
var sizeMenu = document.getElementById("sizeMenu");
var languageButton = document.getElementById("languageMenuButton");
var languageMenu = document.getElementById("languageMenu");
var hideLayers = document.getElementById("nav-layers");
var font_multiplier = 1.0;

//==============================================================================
//  MAP
//==============================================================================

// Init map
mapboxgl.accessToken = mapbox_accessToken;
var map = new mapboxgl.Map({
  container: "map",
  style: mapbox_style, // external style.json
  center: [11.567323, 48.149129], // starting position [lng, lat]
  zoom: 12 // starting zoom
});

// add map controlls
map.addControl(new mapboxgl.ScaleControl(), "bottom-right");
map.addControl(new mapboxgl.NavigationControl(), "bottom-right");
map.addControl(new MapboxGeocoder({ accessToken: mapboxgl.accessToken }), "top-right");

//==============================================================================
//  INTERACTION DATA
//==============================================================================

var spectrum_options = {
  preferredFormat: "rgb",
  showInput: true,
  allowEmpty: false,
  showAlpha: true,
  showButtons: false,
  showPalette: true,
  palette: [],
  maxSelectionSize: 9,
  theme: "sp-cp"
};

var general_layers = [{
  name: "background",
  color: "#dedede",
  layers: [{
    id: "background",
    prop: "background-color"
  }, {
    id: "bridge_minor case",
    prop: "line-color"
  }, {
    id: "bridge_major case",
    prop: "line-color"
  }]
}, {
  name: "park",
  color: "#d2edae",
  layers: [{
    id: "landuse_park",
    prop: "fill-color"
  }, {
    id: "landuse_overlay_national_park",
    prop: "fill-color"
  }]
}, {
  name: "water",
  color: "#a0cfdf",
  layers: [{
    id: "water",
    prop: "fill-color"
  }, {
    id: "waterway",
    prop: "line-color"
  }]
}, {
  name: "building",
  color: "#d6d6d6",
  layers: [{
    id: "building",
    prop: "fill-color"
  }, {
    id: "building_extrude",
    prop: "fill-extrusion-color",
    darken: true
  }]
}, {
  name: "road",
  color: "#fff",
  layers: [{
    id: "tunnel_minor",
    prop: "line-color",
    darken: true
  }, {
    id: "tunnel_major",
    prop: "line-color"
  }, {
    id: "road_minor",
    prop: "line-color",
    darken: true
  }, {
    id: "road_major",
    prop: "line-color"
  }, {
    id: "bridge_minor",
    prop: "line-color",
    darken: true
  }, {
    id: "bridge_major",
    prop: "line-color"
  }]
}];

var landcover_layers = [{
  name: "wood",
  color: "rgba(210, 132, 81, 0.1)",
  layers: [{
    id: "wood",
    prop: "fill-color"
  }]
}, {
  name: "scrub",
  color: "rgba(120, 220, 116, 0.1)",
  layers: [{
    id: "scrub",
    prop: "fill-color"
  }]
}, {
  name: "grass",
  color: "rgba(177, 220, 116, 0.1)",
  layers: [{
    id: "grass",
    prop: "fill-color"
  }]
}, {
  name: "crop",
  color: "rgba(217, 220, 116, 0.1)",
  layers: [{
    id: "crop",
    prop: "fill-color"
  }]
}, {
  name: "snow",
  color: "rgba(116, 220, 220, 0.1)",
  layers: [{
    id: "snow",
    prop: "fill-color"
  }]
}];

var text_layers = [{
  name: "text",
  color: "#666",
  layers: [{
    id: "poi_label",
    prop: "text-color",
    size: function size() {
      return 11 * font_multiplier;
    }
  }, {
    id: "road_major_label",
    prop: "text-color",
    size: function size() {
      return { "base": 1.4, "stops": [[10, 8 * font_multiplier], [20, 14 * font_multiplier]] };
    }
  }, {
    id: "place_label_other",
    prop: "text-color",
    size: function size() {
      return { "stops": [[6, 12 * font_multiplier], [12, 16 * font_multiplier]] };
    }
  }, {
    id: "place_label_city",
    prop: "text-color",
    size: function size() {
      return { "stops": [[3, 12 * font_multiplier], [8, 16 * font_multiplier]] };
    }
  }, {
    id: "country_label",
    prop: "text-color",
    size: function size() {
      return { "stops": [[3, 14 * font_multiplier], [8, 22 * font_multiplier]] };
    }
  }]
}, {
  name: "halo",
  color: "rgba(255,255,255,0.75)",
  layers: [{
    id: "poi_label",
    prop: "text-halo-color"
  }, {
    id: "road_major_label",
    prop: "text-halo-color"
  }, {
    id: "place_label_other",
    prop: "text-halo-color"
  }, {
    id: "place_label_city",
    prop: "text-halo-color"
  }, {
    id: "country_label",
    prop: "text-halo-color"
  }]
}];

// :warning: change these also in google fonts import header
var font_stack = [{
  family: "sans-serif",
  fonts: ["Roboto", "Montserrat", "Open Sans", "Oswald"]
}, {
  family: "serif",
  fonts: ["Roboto Slab", "Merriweather", "Playfair Display"]
}, {
  family: "monospace",
  fonts: ["Roboto Mono", "Ubuntu Mono", "Source Code Pro"]
}, {
  family: "other",
  fonts: ["Caveat", "Orbitron"]
}];

var label_languages = [{
  name: "english",
  text: "{name_en}"
}, {
  name: "deutsch",
  text: "{name_de}"
}, {
  name: "local",
  text: "{name}"
}, {
  name: "local + english",
  text: ["case", ["==", ["string", ["get", "name"]], ["string", ["get", "name_en"]]], ["get", "name"], ["concat", ["get", "name"], "\n(", ["get", "name_en"], ")"]]
}];

var font_sizes = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0];

var hide_layers = [{
  id: "border",
  name: "National borders",
  layers: ["admin_country"],
  checked: true
}, {
  id: "hillshade",
  name: "Hillshade",
  layers: ["hillshade_shadow", "hillshade_highlight"],
  checked: true
}, {
  id: "contour",
  name: "Contour lines",
  layers: ["contour_index", "contour_major", "contour_label"],
  checked: false
}, {
  id: "building3d",
  name: "3D Buildings",
  layers: ["building_extrude"],
  checked: false
}, {
  id: "rails",
  name: "Rails",
  layers: ["road_rail", "road_rail tracks"],
  checked: true
}, {
  id: "poi",
  name: "POIs",
  layers: ["poi_label"],
  checked: true
}];

//==============================================================================
//  INTERACTION FUNCTION
//==============================================================================

function dom_map_colorpicker(layers, parent) {

  layers.forEach(function (layer) {

    var col = document.createElement("div");
    col.classList.add("col-md", "mt-1");

    var label = document.createElement("span");
    label.classList.add("ml-2");
    label.innerHTML = layer.name.charAt(0).toUpperCase() + layer.name.slice(1);

    var input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("id", "color_" + layer.name);

    col.appendChild(input);
    col.appendChild(label);

    var parent_dom = document.getElementById(parent);
    parent_dom.firstElementChild.appendChild(col);

    function change_color(color) {
      var colorRGB = color.toRgbString();
      var colorRGB_darken = color.darken().toRgbString();
      layer.layers.forEach(function (l) {
        map.setPaintProperty(l.id, l.prop, l.darken ? colorRGB_darken : colorRGB);
      });
    }

    $("#color_" + layer.name).spectrum(Object.assign({}, spectrum_options, {
      color: layer.color,
      change: function change(color) {
        change_color(color);
      },
      move: function move(color) {
        change_color(color);
      }
    }));
  });
}

dom_map_colorpicker(general_layers, "nav-general");
dom_map_colorpicker(landcover_layers, "nav-landcover");
dom_map_colorpicker(text_layers, "nav-text");

font_stack.forEach(function (fontfam) {
  var head = document.createElement("h6");
  head.classList.add("dropdown-header");
  head.innerHTML = fontfam.family;
  fontMenu.appendChild(head);

  fontfam.fonts.forEach(function (font) {
    var btn = document.createElement("button");
    btn.classList.add("dropdown-item");
    btn.setAttribute("style", "font-family:" + font + "," + fontfam.family + ";");
    btn.innerHTML = font;
    btn.addEventListener("click", function () {
      fontButton.innerHTML = font;
      fontButton.setAttribute("style", "font-family:" + font + "," + fontfam.family + ";");
      map.setLayoutProperty("poi_label", "text-font", [font + " Regular", "Arial Unicode MS Regular"]);
      map.setLayoutProperty("road_major_label", "text-font", [font + " Bold", "Arial Unicode MS Bold"]);
      map.setLayoutProperty("place_label_other", "text-font", [font + " Regular", "Arial Unicode MS Regular"]);
      map.setLayoutProperty("place_label_city", "text-font", [font + " Bold", "Arial Unicode MS Bold"]);
      map.setLayoutProperty("country_label", "text-font", [font + " Regular", "Arial Unicode MS Regular"]);
    });
    fontMenu.appendChild(btn);
  });
});

font_sizes.forEach(function (fsize) {
  var btn = document.createElement("button");
  btn.classList.add("dropdown-item");
  btn.innerHTML = fsize.toFixed(1);
  btn.addEventListener("click", function () {
    font_multiplier = fsize;
    sizeButton.innerHTML = fsize.toFixed(1);
    text_layers[0].layers.forEach(function (l) {
      map.setLayoutProperty(l.id, "text-size", l.size());
    });
  });
  sizeMenu.appendChild(btn);
});

label_languages.forEach(function (lang) {
  var btn = document.createElement("button");
  btn.classList.add("dropdown-item");
  btn.innerHTML = lang.name;
  btn.addEventListener("click", function () {
    languageButton.innerHTML = lang.name;
    text_layers[0].layers.forEach(function (l) {
      map.setLayoutProperty(l.id, "text-field", lang.text);
    });
  });
  languageMenu.appendChild(btn);
});

hide_layers.forEach(function (layer) {

  var col = document.createElement("div");
  col.classList.add("col-md", "mt-1");

  var container = document.createElement("div");
  container.classList.add("custom-control", "custom-checkbox");

  var input = document.createElement("input");
  input.setAttribute("type", "checkbox");
  input.classList.add("custom-control-input");
  input.setAttribute("id", layer.id);
  input.checked = layer.checked;

  var label = document.createElement("label");
  label.classList.add("custom-control-label");
  label.setAttribute("for", layer.id);
  label.innerHTML = layer.name;

  input.addEventListener("change", function () {
    if (this.checked) {
      // Checkbox is checked
      layer.layers.forEach(function (l) {
        map.setLayoutProperty(l, "visibility", "visible");
      });
    } else {
      // Checkbox is NOT checked
      layer.layers.forEach(function (l) {
        map.setLayoutProperty(l, "visibility", "none");
      });
    }
  });

  container.appendChild(input);
  container.appendChild(label);
  col.appendChild(container);
  hideLayers.firstElementChild.appendChild(col);
});
