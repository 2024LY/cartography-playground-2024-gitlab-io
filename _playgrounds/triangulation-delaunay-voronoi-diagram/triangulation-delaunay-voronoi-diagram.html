---
title: "Delaunay Triangulation and Voronoi Diagram"
description: "Generate the Delaunay Triangulation and the Voronoi Diagram for a set of points."
date: 2020-05-06
tags: douglas peucker ramer line smoothing simplification generalization generalisation
references:

  - author: "Franz Aurenhammer, Rolf Klein, Der-Tsai Lee"
    title: "Voronoi Diagrams and Delaunay Triangulation. World Scientific"
    date: 2013
    link: https://books.google.de/books?hl=de&lr=&id=cic8DQAAQBAJ&oi=fnd&pg=PR5&dq=voronoi+diagrams+and+delaunay+triangulations&ots=O7pZlG8EDz&sig=z5d1wRFEdsVRbf-9o2agA-pYwvw#v=onepage&q=voronoi%20diagrams%20and%20delaunay%20triangulations&f=false
  - author: "Ivan Kutskir"
    title: "Voronoi Diagram in JavaScript"
    date: 2009, accessed July 2020
    link: http://blog.ivank.net/voronoi-diagram-in-javascript.html
  - author: "Rolf Klein"
    title: "Algorithmische Geometrie: Grundlagen, Methoden, Anwendungen. Springer"
    date: 2005
    link: https://books.google.de/books?hl=de&lr=&id=EywlBAAAQBAJ&oi=fnd&pg=PA1&dq=methoden+der+algorithmischen+geometrie&ots=8jkJcO78wk&sig=br3CkR7-pa-_ffXaML99R0RjUIs#v=onepage&q=methoden%20der%20algorithmischen%20geometrie&f=false
  - author: "Samuel Peterson"
    title: "Computing Constrained Delaunay Triangulations"
    date: 1998, accessed July 2020
    link: http://www.geom.uiuc.edu/~samuelp/del_project.html
foot_additional: >
  {% include mathjax.html %}
  {% include svgjs.html %}
  <script src="{{ 'dtvd.js' | prepend: page.url | absolute_url }}"></script>
---

<!-- these files are written by Ivan Kutskir -->
		<script type="text/javascript" src="Point.js"></script> 
		<script type="text/javascript" src="VEdge.js"></script> 
		<script type="text/javascript" src="VEvent.js"></script> 
		<script type="text/javascript" src="VParabola.js"></script> 
		<script type="text/javascript" src="VQueue.js"></script>
		<script type="text/javascript" src="VPolygon.js"></script>
		<script type="text/javascript" src="Voronoi.js"></script> 
		

<div class="row">
  <div class="col">
    <p>
      Using the <em>Delaunay triangulation</em> method - named after Boris Nikolajewitsch Delone - a set of points is combined to form an area-wide network of triangles. <br>
	  The Problem is that there are several ways by which to triangulate any given set of points, but not all meet the requirements for the <em>Delaunay triangulation</em>. 
	  Conditions are that all three points of a triangle lie on a circle within which there are no further points and that therefore they are as “well-shaped” as possible, i.e. at best, equilateral triangles are formed. <br>
	  For the following example set three triangulation variants are possible, but only the first meets the requirements for a <em>Delaunay triangulation</em>. 
	  The other variants are showing at minimum one point (green) inside the circle through the three points of one of the triangles. <br>
	</p>
	<center>
	{% include_relative imgs/good_configuration_delaunay_x.svg class="img-fluid mx-auto d-block" %}
	{% include_relative imgs/bad_configuration_no_delaunay_1_x.svg class="img-fluid mx-auto d-block" %}
	{% include_relative imgs/bad_configuration_no_delaunay_2_x.svg class="img-fluid mx-auto d-block" %}
	</center>
	<br>
	<p>
	  The <em>Delaunay triangulation</em> is used, for example, in digital terrain models to create a TIN data structure. <br>
	  The <em>Voronoi diagram</em> (also <em>Thiessen polygons</em> or <em>Dirichlet tessellation</em>) is known as the so-called dual graph of the <em>Delaunay triangulation</em>. 
	  It was named after Georgi Feodosjewitsch Woronoi and enables the subdivision of surfaces into areas of influence. <br>
	  The <em>Voronoi diagram</em> is used, for example, in robotics for route planning with existing obstacles or in zoology as a model and analysis of animal territories.
    </p>
  </div>
</div>

<h2 class="mt-5">Algorithm</h2>
<hr>
<p>
  Given is a set of \( n \) unique points \( P \) in one level
  \[ P = (p_1, p_2, p_3, \cdots, p_n) \]
</p>

<center>
{% include_relative imgs/point_set.svg class="img-fluid mx-auto d-block" %}
</center>
<br>

<p>
  The <em>Voronoi diagram</em> of \( P \) is defined as subdividing the level into \( n \) cells. 
  For each point in \( P \), a cell is created with the property that a point \( q \) is only in the cell belonging to \( p_i \) if:
  \[ dist(q,p_i) < dist(q,p_j) \] for every point \(p_j \in P\) with \(j \neq i\). <br>
  So, the Voronoi edges are constructed by creating the perpendicular bisector of the line between two points respectively and blending them.
</p>

<center>
{% include_relative imgs/voronoi.svg class="img-fluid mx-auto d-block" %}
</center>
<br>

<p>
  The <em>Delaunay triangulation</em> of \(P\) is then created by connecting the points of all neighboring Voronoi cells.
</p>

<center>
{% include_relative imgs/delaunay.svg class="img-fluid mx-auto d-block" %}
</center>
<br>

<p>
   In the end the combination of <em>Delaunay triangulation</em> and <em>Voronoi diagram</em> looks like this.
</p>

<center>
{% include_relative imgs/delaunay_and_voronoi.svg class="img-fluid mx-auto d-block" %}
</center>
<br>

<p>
   So, the Voronoi cell arises from the intersection of the perpendicular bisectors of the Delaunay triangle sides that belong together.
</p>

<h2 class="mt-5">Hands-On</h2>
<hr>

<div class="container mw-900">
	<div class="row mb-3">
		<div class="col">Click in the box to generate points. On the basis of these points the <em>Delaunay triangulation</em> and the <em>Voronoi diagram</em> will be calculated. You can clear the points by clicking on the <em>Clear</em> Button or you can return to the default point set by clicking the <em>Reset</em> Button. Change the Toggles for seeing only the <em>Delaunay triangulation (orange)</em> or only the <em>Voronoi diagram (blue)</em> or both. </div>
	</div>

	<div class="card shadow">
		<div class="card-header h5">
			Delaunay triangulation and Voronoi diagram
		</div>
		<div class="card-body p-3">

			<div class="row d-flex align-items-center">

				<div class="col-md-2">
					<div class="custom-control custom-switch mr-2 mb-1">
						<input type="checkbox" class="custom-control-input" id="check-voronoi" checked>
						<label class="custom-control-label" for="check-voronoi" data-toggle="tooltip" title="Display/hide Voronoi diagram">Voronoi</label>
					</div>
				</div>	

				<div class="col-md-2">
					<div class="custom-control custom-switch">
						<input type="checkbox" class="custom-control-input" id="check-delaunay">
						<label class="custom-control-label" for="check-delaunay" data-toggle="tooltip" title="Display/hide Delaunay triangulation">Delaunay</label>
					</div>
				</div>
				
				<div class="col-md-5">
				</div>

				<div class="col-md-3 text-rigth" >
					<button type="button" id="dtvd-clear" class="btn btn-sm btn-warning m-1" data-toggle="tooltip" title="Delete all points" ><i class="mdi mdi-delete mr-1"></i>Clear</button>
					<div class="btn-group m-1">
						<button type="button" id="dtvd-reset" class="btn btn-sm btn-secondary border-right" data-toggle="tooltip" title="Return to default point set"><i class="mdi mdi-undo-variant mr-1"></i>Reset</button>
					</div>
				</div>
				
			</div>
		</div>
		<div id="drawing" class="card-img-bottom bg-light border-top"></div>	
	</div>
	
</div>


